
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h> 
#include <arpa/inet.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <iostream>
#include <string.h>
#include "../shared/RDTManager.h"
#include "../shared/ApplicationLayer.h"


void exitWithError(std::string errorMessage)
{
	std::cout << errorMessage << std::endl;
	std::cout << "Exiting." << std::endl;
	exit(0);
}


int main(int argc, char const *argv[])
{
    
    //Declare parameters
    int portNumber;
    std::string senderHostName;
    std::string filename;
    float pktLossProb;
    float pktCorruptProb;
    char inputBuffer[10000];

    fd_set activeFdSet;
    std::string output;
    int socketFd;
    int cwnd;
    RDTManager* rdtManager;

    //Parse program arguments
    if (argc != 5)
    {
    	exitWithError("Incorrect Usage. Requires sender <portnumber> <CWnd> <Pl> <Pc>");
    }

    portNumber      	= atoi(argv[1]);
    cwnd       		    = atoi(argv[2]);
    pktLossProb 		= atof(argv[3]);
    pktCorruptProb 		= atof(argv[4]);

    std::cout << "Starting sender..." << std::endl;

    rdtManager = new RDTManager(pktLossProb, pktCorruptProb, cwnd);
    socketFd = rdtManager->createSocket();

    if(socketFd < 0)
    {
        exitWithError("Could not create socket");
    }

    if(rdtManager->serverBind(socketFd, portNumber) < 0)
    {
        exitWithError("Could not bind to port.");
    }
    std::string requestOutput;
    char applicationBuffer[1000];

    if(rdtManager->serverListenAndAccept(socketFd) < 0) //listen for an incoming connection
    {
        exitWithError("Error Listening and Accepting.");
    }

    std::cout << "Established connection with client..." << std::endl;

    char outputBuffer [1000];
    int bytes=1000;
    SenderApplication sApp;

    // Read the request from the receiver: "GETFILE <filename"
    rdtManager->readFromSocket(socketFd, inputBuffer, 1000); //will be blocked until something is filled into response buffer    
    // Process the request. outputBuffer = "OK 8" or "ERROR: File not found"
    bytes=1000;
    sApp.process (inputBuffer,outputBuffer,&bytes);
    std::cout << "in= " << inputBuffer << "out=" << outputBuffer << std::endl;
    rdtManager->writeToSocket(socketFd, outputBuffer, bytes);
    while(1)
    {
        // Process the request. Fill the outputBuffer with what needs to be sent to receiver.
        bytes=1000;
        sApp.process (inputBuffer,outputBuffer,&bytes);    
        std::cout << "out=" << outputBuffer <<std::endl;
        rdtManager->writeToSocket(socketFd, outputBuffer, bytes);
    }


    /*
    if (bind(socketFd, (struct sockaddr*) &serverAddr, sizeof(struct sockaddr_in)) < 0)
    {
        cout << "Error binding. Exiting." << endl;
        return -1;
    }

    if (listen(socketFd,MAX_QUEUED_CONNECTIONS) < 0)
    {
        cout << "Error listening. Exiting." << endl;
        return -1;
    }

    //Manage activeFdSet 
    FD_ZERO(&activeFdSet);
    FD_SET(socketFd, &activeFdSet);

    //main loop
    cout << "Web server started." << endl;
    cout << "Accepting HTTP connections on port "<< SERVER_PORT << "..." << endl << endl;
    while(1)
    {
        if(select(socketFd + 1, &activeFdSet, NULL, NULL, NULL) < 0)
        {
            cout << "Socket error. Exiting." << endl;
            exit(-1);
        }

        if(FD_ISSET(socketFd, &activeFdSet))
        {
            //Get and set new socket in the active FD set.
            newSocket = accept(socketFd, (struct sockaddr *) &clientAddr, &clientLength);

            if (newSocket < 0)
            {
                cout << "Error accepting. Exiting." << endl;
                return -1;
            }

            FD_SET(newSocket, &activeFdSet);
        }

        if(FD_ISSET(newSocket, &activeFdSet))
        {
            hr.readRequest(newSocket);
            hr.logRequestToConsole();
            hr.processRequest();
            close(newSocket);
        }
    }
    */


    close(socketFd);
    return 0;
}
