#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h> 
#include <arpa/inet.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <iostream>
#include <string.h>
#include "../shared/RDTManager.h"
#include "../shared/ApplicationLayer.h"

void exitWithError(std::string errorMessage)
{
	std::cout << errorMessage << std::endl;
	std::cout << "Exiting." << std::endl;
	exit(0);
}

int main(int argc, char **argv)
{
    
    //Declare parameters
    int senderPortNumber;
    std::string senderHostName;
    std::string filename;
    float pktLossProb;
    float pktCorruptProb;
    sockaddr_in serverAddr, clientAddr;
    socklen_t clientLength;
    fd_set activeFdSet;
    std::string output;
    int socketFd;
    RDTManager* rdtManager;

    //Parse program arguments
    if (argc != 6)
    {
    	exitWithError("Incorrect Usage. Requires receiver <sender_hostname> <sender_portno> <filename> <Pl> <Pc>");
    }

    senderHostName      = argv[1];
    senderPortNumber 	= atoi(argv[2]);
    filename 			= argv[3];
    pktLossProb 		= atof(argv[4]);
    pktCorruptProb 		= atof(argv[5]);

    std::cout << std::endl;
    std::cout << "Sender Hostname: " << senderHostName << std::endl;
    std::cout << "Sender Port Number: " << senderPortNumber << std::endl;
    std::cout << "Requested Filename: " << filename << std::endl;
    std::cout << "Packet Loss Probability: " << pktLossProb << std::endl;
    std::cout << "Packet Corruption Probability: " << pktCorruptProb << std::endl;
    std::cout << std::endl;

    std::cout << "Starting receiver..." << std::endl;
    //set server address

    rdtManager = new RDTManager(pktLossProb,pktCorruptProb, RDTManager::MAX_PACKET_SIZE);
    socketFd = rdtManager->createSocket();
    if(socketFd < 0)
    {
        exitWithError("Could not create socket.");
    }

    //Connect to server
    if(rdtManager->receiverConnect(socketFd, senderPortNumber, senderHostName) < 0) 
    {
        exitWithError("Could not connect to server.");
    }

    std::cout << "Established connection to server..." << std::endl;
    
    ReceiverApplication rApp (filename.c_str());
    char inBuff[1000], outBuff[1000];
    int bytes = 1000;
    // Frame the request "GETFILE <filename>" & send it to the sender
    rApp.process(inBuff, outBuff, &bytes);
    std::cout << " outBuff = " << outBuff << std::endl;
    rdtManager->writeToSocket(socketFd, outBuff, bytes);
    
    // Get response from the sender, "OK 8" or "ERROR: File not found"
    rdtManager->readFromSocket(socketFd, inBuff, 1000); //will be blocked until something is filled into response buffer
    std::cout << "inBuff = " << inBuff <<std::endl;
    rApp.process (inBuff,outBuff,&bytes);
    

    while (rApp.mBytesProcessed < rApp.mFileSize)
    {
        rdtManager->readFromSocket(socketFd, inBuff, 1000); //will be blocked until something is filled into response buffer
        std::cout << "inBuff ="<<inBuff << std::endl;
        rApp.process (inBuff,outBuff,&bytes);    
    }




    /*
    if (bind(socketFd, (struct sockaddr*) &serverAddr, sizeof(struct sockaddr_in)) < 0)
    {
        cout << "Error binding. Exiting." << endl;
        return -1;
    }

    if (listen(socketFd,MAX_QUEUED_CONNECTIONS) < 0)
    {d
        cout << "Error listening. Exiting." << endl;
        return -1;
    }

    //Manage activeFdSet 
    FD_ZERO(&activeFdSet);
    FD_SET(socketFd, &activeFdSet);

    //main loop
    cout << "Web server started." << endl;
    cout << "Accepting HTTP connections on port "<< SERVER_PORT << "..." << endl << endl;
    while(1)
    {
        if(select(socketFd + 1, &activeFdSet, NULL, NULL, NULL) < 0)
        {
            cout << "Socket error. Exiting." << endl;
            exit(-1);
        }

        if(FD_ISSET(socketFd, &activeFdSet))
        {
            //Get and set new socket in the active FD set.
            newSocket = accept(socketFd, (struct sockaddr *) &clientAddr, &clientLength);

            if (newSocket < 0)
            {
                cout << "Error accepting. Exiting." << endl;
                return -1;
            }

            FD_SET(newSocket, &activeFdSet);
        }

        if(FD_ISSET(newSocket, &activeFdSet))
        {
            hr.readRequest(newSocket);
            hr.logRequestToConsole();
            hr.processRequest();
            close(newSocket);
        }
    }
    */


    close(socketFd);
    return 0;
}
