#ifndef __RDT_H__ 
#define __RDT_H__ 

#include <string>
#include <signal.h>
#include <stdio.h> 
#include <stdlib.h>
#include <vector>
#include <time.h> 


class RDTManager {

	public:
	//Constants
	static const int MAX_PACKET_SIZE = 1000; //max packet size is 1000 bytes
	static const unsigned int MAX_WAIT_TIME = 1; //second

	//Typedefs and Structs
	struct RDTPacketHeader //Define packet header for communication define to 20 bytes
	{
		double sequenceNumber;
		double ackNumber;
		unsigned int senderSize;
		unsigned int dataSize;
		bool ackFlag;
		bool synFlag;
		bool finFlag;

	};

	struct RDTPacket //used for storing in buffer for processing, not for sending or receiving.
	{
		//Information
		RDTPacketHeader header;
		char data[MAX_PACKET_SIZE - sizeof(RDTPacketHeader)];

		//Meta Data
		bool lost;
		bool corrupt;
		bool acked;
		bool received;
		time_t timeSent;
	};
	

		//Public Functions
		RDTManager(float lossProb, float corruptProb, int windowSize);
		~RDTManager();
		
		//Receiver Functions
		int receiverConnect(int socketFd, int senderPortNumber, std::string hostname);

		//Server Functions
		int serverAccept(int socketFd, RDTPacketHeader* pktHeader, char* buffer);
		int serverBind(int socketFd, int portNumber);
		int serverListenAndAccept(int socketFd);

		//Mutual Functions
		int createSocket();
		ssize_t writeToSocket(int socketFd, char* inBuffer, int inBufferSize);
		ssize_t readFromSocket(int socketFd, char* outputBuffer, int outputBufferSize);
		bool dropPacket ();
		bool corruptPacket ();
		
		int processRequest(); //Parse request and write to socket

	private:

		//Private Functions
		void printPacketHeader(RDTPacketHeader* pktHeader, bool received);
		void setPacketHeader(
							RDTPacketHeader* pktHeader,
							double sequenceNumber,
							double ackNumber,
							unsigned int receiveWindow,
							unsigned int dataSize,
							bool ackFlag,
							bool synFlag,
							bool finFlag
							);
		static void expireTimer(int sig);
		int processQueuedPackets(int socketFd);
		int processPacket(int socketFd, RDTPacketHeader* pktHeader, char* dataSize, int bufferIndex);
		ssize_t sendPacket(int socketFd, RDTPacketHeader* pktHeader, char* data);
		ssize_t readPacketsToBuffer(int socketFd);
		int sendAck(int socketFd, double ackNumber);
		void updateReceiverPacketBufferSize(unsigned int senderSize);
		int senderHandleAckPacket(int socketFd, RDTPacketHeader* pktHeader, char* data, int bufferIndex);
		int receiverHandleDataPacket(int socketFd, RDTPacketHeader* pktHeader, char* data, int bufferIndex);
		int checkTimedOutPackets();
		bool getNextSequenceNumber(bool dataPresent, double dataLeft, int &bytesInPacket, double &sequenceNumber);


		//Member Variables
		sockaddr_in mRemoteAddress;
		int mSockFd;
		int mCwnd;
		int mRemoteCwnd;
		float mPktLossProb;
		float mPktCorruptProb;
		int mHeaderSize;
		char mReceiveBuffer[RDTManager::MAX_PACKET_SIZE];
		char mSendBuffer[RDTManager::MAX_PACKET_SIZE];
		RDTPacket* mSendPacketBuffer;
		RDTPacket* mReceivePacketBuffer;

		std::vector<RDTPacket> mSentPackets; //data packets sent and unacked
		std::vector<RDTPacket> mQueuedPackets; //packets read and queued for processing
		std::vector<RDTPacket> mBufferedPackets; //insequential packets buffered

		unsigned int mSendPacketBufferSize;
		unsigned int mReceivePacketBufferSize;
		unsigned int mLastPacketDataSize;
		unsigned int mNumberOfQueuedPackets;
		unsigned int mNumberOfSentPackets;
		char* mWriteBuffer;
		size_t mWriteBufferOffset;
		double mSenderBase;
		double mReceiverBase;
		double mLastSequenceNumber;
};


#endif
