#include <iostream>
#include <string.h>
#include <string>
#include <fstream>
#include "ApplicationLayer.h"
using namespace std;

void TestReceiverApplication();
void TestSenderApplication();

///////////////////////////////////////////////////////////////////////////////
// Application
///////////////////////////////////////////////////////////////////////////////
Application::Application()
{
    controlMsgGetFile.assign(CONTROL_MSG_GETFILE);
    controlMsgFileNotFound.assign(CONTROL_MSG_FILE_NOT_FOUND);
    controlMsgStatusOk.assign(CONTROL_MSG_STATUS_OK);
    mFileBuffer = 0;
    mFileSize = 0;
    mRootFolder.assign(DEFAULT_FOLDER_LOCATION); /*Need to update*/
    mBytesProcessed = 0;
}
Application::~Application()
{
    mFileSize = 0;
    if (mFileBuffer)
        delete [] mFileBuffer;
}

// Check if the file exists.
bool Application::checkIfFileExists (string fileName)
{
    // A much better way to do this would be to use boost library.
    // without boost, we would have to use Windows API or Ubuntu specific API,
    // which I do not want to do. But, sadly, boostlib is not available in
    // our CS118 unbuntu image.
    // This is how we would do it using boost:
    // #include <boost/filesystem.hpp>
    // using boost::filesystem;
    // if (!exists("lib.dll")) { 
    // std::cout << "dll does not exists." << std::endl;}
    // Check in ubuntu if boost is installed:
    // Check: http://askubuntu.com/questions/263461/where-is-my-boost-lib-file
    string filePath;
    filePath.assign(mRootFolder);
    filePath.append(fileName);
    if (fileName.length() == 0)
        return false;
    ifstream ifile(filePath.c_str());
    if (ifile) 
    {
        // The file exists
        ifile.close();
        return true;
    }
    // File does not exist    
    return false;
}

///////////////////////////////////////////////////////////////////////////////
// Receiver
///////////////////////////////////////////////////////////////////////////////

void ReceiverApplication::printFileRequestMsg()
{
    mControlMsg.assign (controlMsgGetFile);
    mControlMsg.append(" ");
    mControlMsg.append(mRequestedFileName);
}

void ReceiverApplication::readRequestResponse(string response)
{
    if (response.find(controlMsgFileNotFound) != string::npos)
    {
        // Sender replied with "File Not Found"
        mFileSize = -1;
        mNextState = APP_COMPLETE;
    }
    else if (response.find(controlMsgStatusOk) != string::npos)
    {
        // sender replied with "OK <LengthOfFile>"
        string fileSizeStr = response.substr(controlMsgStatusOk.length() + 1, response.length() - controlMsgStatusOk.length() - 1);
        mFileSize = atoi(fileSizeStr.c_str());
        mFileBuffer = new char [mFileSize];
        if (mFileBuffer == 0)
        {
            cout << "Could not allocate buffer" << endl;
        }
        mNextState = APP_READ_DATA;
    }
}
void ReceiverApplication::writeFullBufferToFile (char * fileBuffer)
{
    ofstream outFile;
    string destFilePath (mRootFolder);
    destFilePath.append(mRequestedFileName);
    destFilePath.append("rcv");
    outFile.open (destFilePath.c_str());
    if (outFile.is_open())
    {
        outFile << fileBuffer;
        outFile.close();
    }
    else
    {
        cout << "Error Writing to file: " << destFilePath << endl;
    }
}

void ReceiverApplication::writePartialBufferToFile (char *fileBuffer)
{
    ofstream outFile;
    string destFilePath (mRootFolder);
    destFilePath.append(mRequestedFileName);
    destFilePath.append("rcv");
    if (checkIfFileExists(destFilePath))
    {
        outFile.open (destFilePath.c_str(), ios::out|ios::binary|ios::app);
    }
    else
    {
        outFile.open(destFilePath.c_str(), ios::out|ios::binary);
    }
    if (outFile.is_open())
    {
        outFile << fileBuffer;
        outFile.close();
    }
    else 
    {
        cout << "Error writing to file: " << destFilePath << endl;
    }
}

void ReceiverApplication::process (const char * input, char * output, int * bytes)
{
    switch (mProcessState)
    {
        case APP_READ_CONTROL_MSG:
        {
            string response (input);
            readRequestResponse (response);
            mProcessState = mNextState;
        }
        break;

        case APP_READ_DATA:
        // input = the data sent up from the transport layer.
        // *bytes = number of bytes in input buffer which has to be read.
        if (mBytesProcessed + *bytes <= mFileSize && *bytes > 0)
        {
                strncpy(mFileBuffer + mBytesProcessed, input, *bytes);
                mBytesProcessed += *bytes;
        }
        if (mBytesProcessed == mFileSize)
        {
            writeFullBufferToFile (mFileBuffer);
            mProcessState = APP_COMPLETE; 
        }
        break;

        case APP_SEND_CONTROL_MSG:
        // Example: "GETFILE helloworld.jpg"
        printFileRequestMsg ();
        if (*bytes < mControlMsg.length() + 1)
        {
            *bytes = mControlMsg.length() + 1;
        }
        else if (output != 0)
        {
            strcpy (output, mControlMsg.c_str());    
            mProcessState = APP_READ_CONTROL_MSG;
        }        
        break;

        case APP_SEND_DATA:
        // invalid state
        break;

        case APP_COMPLETE:
        default:
        break;
    }
}
ReceiverApplication::ReceiverApplication(const char * requestedFile)
{
    mRequestedFileName.assign(requestedFile);
    mProcessState = APP_SEND_CONTROL_MSG;
}
ReceiverApplication::ReceiverApplication()
{
    mProcessState = APP_SEND_CONTROL_MSG;
}

ReceiverApplication::~ReceiverApplication()
{

}

///////////////////////////////////////////////////////////////////////////////
// Sender
///////////////////////////////////////////////////////////////////////////////
void SenderApplication::parseRequestMsg(string inputMsg)
{
    mRequestedFileName.assign("");
    if(inputMsg.find (controlMsgGetFile) != string::npos)
    {
        // "GETFILE" found in payload, now look for the <filename>.
        mRequestedFileName=inputMsg.substr(controlMsgGetFile.length()+1, 
            inputMsg.length()-controlMsgGetFile.length()-1);
    }
}

bool SenderApplication::readRequestedFile ()
{
    if (checkIfFileExists(mRequestedFileName))
    {
        // Read the file contents
        ifstream::pos_type size;
        ifstream requestedFile;
        string filePath;
        filePath.assign(mRootFolder);
        filePath.append(mRequestedFileName);
        requestedFile.open (filePath.c_str(), ios::in|ios::binary|ios::ate);
        if (!requestedFile)
        {
            cout << "ERROR: Could not open file " << filePath <<endl;
            return false;
        }   
        size = requestedFile.tellg();
        mFileBuffer = new char [size];
        if (mFileBuffer == 0)
        {
            cout << "ERROR: Could not allocate buffer" << endl;
            return false;
        }
        requestedFile.seekg(0, ios::beg);
        requestedFile.read (mFileBuffer, size);
        requestedFile.close ();
        mFileSize = (int)size;
        // Status OK
        printStatusOK ();
    }
    else
    {
        // Status: Error
        printError ();
    }
}
void SenderApplication::printError ()
{
    mControlMsg.assign(controlMsgFileNotFound);
    mNextState = APP_COMPLETE; /*Discard any more packets*/
}
void SenderApplication::printStatusOK()
{    
    mControlMsg.assign(controlMsgStatusOk);
    mControlMsg.append(" ");
    mControlMsg.append(std::to_string(mFileSize));
    mNextState = APP_SEND_DATA;
}
void SenderApplication::process (const char * input, char * output, int * bytes)
{
    switch (mProcessState)
    {
        case APP_READ_CONTROL_MSG:
        // Example: input="GETFILE helloworld.jpg"
        if (input != 0)
        {
            // We do not care about output or *bytes in this state.
            string inputMsg (input);
            parseRequestMsg (inputMsg);
            mProcessState = APP_SEND_CONTROL_MSG;
        }
        break;

        case APP_READ_DATA:
        // invalid state
        break;

        case APP_SEND_CONTROL_MSG:
        // In this state, if *bytes=0, we reply back to the caller
        // with the required number of bytes needed to handle the message
        // We expect the caller to allocate *bytes amount of space to output
        // If output is allocated, we print the payload msg to output.

        // Sample output
        // mControlMsg="OK 474" or "ERROR: File not found"
        // *bytes=7 or 21
        readRequestedFile ();
        if (*bytes >= (mControlMsg.length() + 1))
        {
            if (output != 0)
            {
                strcpy (output, mControlMsg.c_str());
                mProcessState = mNextState;
            }
        }
        else
        {
            // Dear caller, we want (*bytes)bytes to be allocated to output
            *bytes = mControlMsg.length() + 1;
        }
        
        break;

        case APP_SEND_DATA:
        // In this state, *bytes would represent the number of bytes of the
        // data that is required.
        // It is assumed that output can store *bytes amount of data.
        if (mBytesProcessed <= mFileSize && *bytes > 0)
        {
            if (output != 0)
            {
                int bytesToRead = *bytes > (mFileSize - mBytesProcessed) ? (mFileSize - mBytesProcessed) : *bytes;
                strncpy (output, mFileBuffer + mBytesProcessed, bytesToRead);
                mBytesProcessed += bytesToRead;
            }
        }
        if (mBytesProcessed == mFileSize)
        {
            mProcessState = APP_COMPLETE;
        }
        break;

        case APP_COMPLETE:
        default:
        break;
    }
}

// constructor
SenderApplication::SenderApplication()
{
    mProcessState = APP_READ_CONTROL_MSG;
}
// Destructor
SenderApplication::~SenderApplication()
{
 
}
///////////////////////////////////////////////////////////////////////////////
// main
///////////////////////////////////////////////////////////////////////////////
/*
int main ()
{
    //TestReceiverApplication();
    //TestSenderApplication();
}*/

void TestReceiverApplication()
{
    char input [1000], output[1000];
    int bytes=1000;
    //string requestedFile ("hello.jpg");
    strcpy(input, "hello.jpg");
    ReceiverApplication rApp(input);
    // This would fill the output buffer with "GETFILE hello.jpg"
    // This output buffer is sent to Sender.
    rApp.process (input, output, &bytes);
    
    // Now, sender responds with "OK 8", meaning
    // sender found the file, and it is 8 bytes long.
    // otherwise, sender would have sent "ERROR: File not found"
    strcpy(input,"OK 8");
    rApp.process(input, output, &bytes);
    
    // Now, you want to tell the receiver, please store these 4 bytes
    strcpy(input, "abcd");
    bytes=4;
    rApp.process(input, output, &bytes);
   
   // Now, store the next 4 bytes
   strcpy(input, "efgh");
   rApp.process (input, output, &bytes); 
   
   // At this point, the receiver will stop receiving any more bytes, and 
   // would have already written buff to file.
   rApp.process(input, output, &bytes); //this will have no effect.
    

}

void TestSenderApplication()
{
    char input [1000], output[1000];
    int bytes=1000;
    SenderApplication sApp;
    
    // At first, the Sender receives the request from the receiver
    // which is "GETFILE <filename>" 
 
    strcpy(input, "GETFILE hello.jpgrcv");
    sApp.process(input,output, &bytes);
 
    // The server will update output buffer
    // output = "OK <bytes>" (<bytes> is the size of the file) if it found the file
    // output = "ERROR: File not found" if Sender could not find the file   
    sApp.process(input,output, &bytes);
    
    // the server will update the output buffer with 1000 bytes of data (or) fileSize, 
    // whichever is lesser.
    sApp.process(input,output, &bytes);
}
