#ifndef APPLICATIONLAYER_H
#define APPLICATIONLAYER_H

#include <string>
#define DEFAULT_FOLDER_LOCATION "/home/lakshman/projects/"
#define CONTROL_MSG_STATUS_OK "OK"
#define CONTROL_MSG_GETFILE "GETFILE"
#define CONTROL_MSG_FILE_NOT_FOUND "ERROR: File not Found"

class Application
{
    public:
    Application ();
    ~Application();
    virtual void process (const char * input, char * output, int * bytes) = 0;
    public:
    int mFileSize;
    int mBytesProcessed;
    
    protected:
    enum ApplicationState
    {
        APP_READ_CONTROL_MSG,
        APP_READ_DATA,
        APP_SEND_CONTROL_MSG,
        APP_SEND_DATA,
        APP_COMPLETE
    };
    bool checkIfFileExists (std::string fileName);
    std::string controlMsgGetFile;
    std::string controlMsgFileNotFound; 
    std::string controlMsgStatusOk;
    char *mFileBuffer; /*Stores the contents of the file*/

    std::string mRequestedFileName;
    std::string mRootFolder; /*This is where we look for mRequestedFileName */
    enum ApplicationState mProcessState;
    enum ApplicationState mNextState;
    std::string mControlMsg; 
};

class SenderApplication : public Application
{
    public:
    SenderApplication();
    ~SenderApplication();
    void process (const char * input, char * output, int * bytes);

    private:
    void parseRequestMsg (std::string input);
    bool readRequestedFile ();    
    void printError ();
    void printStatusOK ();


    public:
    
    
    
};

class ReceiverApplication: public Application
{
    public:
    ReceiverApplication ();
    ReceiverApplication (const char * requestedFile);
    ~ReceiverApplication();
    void process (const char * input, char * output, int * bytes);

    private:
    void printFileRequestMsg ();
    void readRequestResponse (std::string response);    
    void writePartialBufferToFile (char *fileBuffer);
    void writeFullBufferToFile(char * fileBuffer);

};


#endif
