#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h> 
#include <arpa/inet.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <iostream>
#include <string.h>
#include <netdb.h>
#include <sys/time.h>
#include <signal.h>
#include <sys/fcntl.h>
#include <vector>
#include "RDTManager.h"	

RDTManager::RDTManager(float lossProb, float corruptProb, int windowSize)
{
	mPktLossProb = lossProb;
	mPktCorruptProb = corruptProb;
	mCwnd = windowSize;

	mHeaderSize = sizeof(RDTPacketHeader);

	std::cout << "Size of Header Packet is: " << mHeaderSize << " bytes." << std::endl;
	memset(&mRemoteAddress, 0, sizeof(mRemoteAddress));
	memset(&mSendBuffer, 0, RDTManager::MAX_PACKET_SIZE);
	memset(&mReceiveBuffer, 0, RDTManager::MAX_PACKET_SIZE);

	mSenderBase = 0;
	mReceiverBase = 0;
	mLastSequenceNumber = 0;

	mNumberOfSentPackets = 0;

	mNumberOfQueuedPackets = 0;
	double calculatedLastFrame = windowSize % RDTManager::MAX_PACKET_SIZE;
	mLastPacketDataSize = RDTManager::MAX_PACKET_SIZE - mHeaderSize;
	if(calculatedLastFrame > mHeaderSize)
		mLastPacketDataSize = windowSize % RDTManager::MAX_PACKET_SIZE - mHeaderSize;

	mSendPacketBufferSize = windowSize/RDTManager::MAX_PACKET_SIZE;
	mReceivePacketBufferSize = 1;
	mRemoteCwnd = mReceivePacketBufferSize * RDTManager::MAX_PACKET_SIZE;

	if(mLastPacketDataSize < RDTManager::MAX_PACKET_SIZE - mHeaderSize) //check if we will need one extra packet to send extra data
	{								
		mSendPacketBufferSize++;
	}

	mSendPacketBuffer = new RDTPacket[mSendPacketBufferSize];
	mReceivePacketBuffer = new RDTPacket[mReceivePacketBufferSize]; //Initially only receive on packet at a time until handshake.

	std::cout << "Send Packet Buffer Size is: " << mSendPacketBufferSize << " packets." << std::endl;

	std::cout << "The last packet should contain: " << mLastPacketDataSize << " bytes of data." << std::endl;

}

RDTManager::~RDTManager()
{
	delete[] mSendPacketBuffer;
	delete[] mReceivePacketBuffer;
}

int RDTManager::createSocket()
{
	int socketFd;

	socketFd = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
	return socketFd;
}


//Connect to server
int RDTManager::receiverConnect(int socketFd, int senderPortNumber, std::string hostname)
{
	 struct hostent* host;
	 RDTPacketHeader pktHeader;

	 //Get hostname
	 host = gethostbyname(hostname.c_str());

	 if(host == NULL)
	 {
	 	std::cout << "Could not get hostname" << std::endl;
	 	return -1;
	 }

	//set mRemoteAddress to sender information
    mRemoteAddress.sin_family = AF_INET;
    mRemoteAddress.sin_port = htons(senderPortNumber);
 	memcpy(&mRemoteAddress.sin_addr, host->h_addr_list[0], host->h_length);
 	double sequenceNumber;
 	int bytesInPacket;

 	getNextSequenceNumber(false, 0, bytesInPacket, sequenceNumber);
 	//Populate SYN packet header to send to sender
 	setPacketHeader(&pktHeader, sequenceNumber, 0, mCwnd, 0, false, true, false);

 	ssize_t bytesWritten = 0;
	fd_set activeFdSet;
	fd_set readSet;
	fd_set writeSet;

	FD_ZERO(&activeFdSet);
	FD_ZERO(&readSet);
	FD_ZERO(&writeSet);
	FD_SET(socketFd, &activeFdSet);
	FD_SET(socketFd, &readSet);
	FD_SET(socketFd, &writeSet);

	std::cout << "Waiting to send SYN Packet" << std::endl;
	if(select(socketFd + 1, NULL, &writeSet, NULL, NULL) < 0)
    {
        std::cout << "Socket error. Exiting." << std::endl;
        return -1;
    }

    if(FD_ISSET(socketFd, &writeSet))
    {
    	if(sendPacket(socketFd, &pktHeader, NULL) >= 0)
		{	
			std::cout << "Waiting to receive SYNACK Packet" << std::endl;

			//Wait for SYNACK
			if(select(socketFd + 1, &readSet, NULL, NULL, NULL) < 0)
			{
				std::cout << "Socket error. Exiting." << std::endl;
				return -1;
			}

			if(FD_ISSET(socketFd, &readSet))
			{
				if(readPacketsToBuffer(socketFd) > 0)
				{
					return processQueuedPackets(socketFd);
				}
			}
		}

		else
		{
			std::cout  << "Could not send packet in receiverConnect()" << std::endl;
		}
    }

    else
    {
    	std::cout << "Socket not readable in receiverConnect() " << std::endl;
    }

	return 1;
}

int RDTManager::serverBind(int socketFd, int portNumber)
{

	sockaddr_in serverAddress;

	serverAddress.sin_family = AF_INET;
    serverAddress.sin_addr.s_addr = htonl(INADDR_ANY);
    serverAddress.sin_port = htons(portNumber);

    socklen_t fromSize = sizeof mRemoteAddress;

	return bind(socketFd, (struct sockaddr*) &serverAddress, fromSize);
}

int RDTManager::serverListenAndAccept(int socketFd)
{

	fd_set activeFdSet;
	fd_set readSet;
	fd_set writeSet;

	FD_ZERO(&activeFdSet);
	FD_ZERO(&readSet);
	FD_ZERO(&writeSet);
	FD_SET(socketFd, &activeFdSet);
	FD_SET(socketFd, &readSet);

	if(select(socketFd + 1, &readSet, NULL, NULL, NULL) < 0)
    {
        std::cout << "Socket error. Exiting." << std::endl;
        return -1;
    }

    if(FD_ISSET(socketFd, &readSet))
    {
    	//Perform the read and process the packet
    	if(readPacketsToBuffer(socketFd) > 0)
    	{
    		return processQueuedPackets(socketFd);
    	}

    	else
    	{
    		std::cout << "Connection could not be established." << std::endl;
    		return -1;
    	}
    }

    return 1;
}

int RDTManager::serverAccept(int socketFd, RDTPacketHeader* pktHeader, char* data)
{
	RDTPacketHeader outputPktHeader;
	double sequenceNumber;
	int bytesInPacket;

	getNextSequenceNumber(false, 0, bytesInPacket, sequenceNumber);
	//Set packet
	setPacketHeader(&outputPktHeader, sequenceNumber, pktHeader->sequenceNumber, mCwnd, 0, true, true, false); //SYNACK
	//reset receive window to sender size
	updateReceiverPacketBufferSize(pktHeader->senderSize);
	sendPacket(socketFd,&outputPktHeader, NULL); //send SYNACK with no data

	fd_set activeFdSet;
	fd_set readSet;

	FD_ZERO(&activeFdSet);
	FD_ZERO(&readSet);
	FD_SET(socketFd, &activeFdSet);
	FD_SET(socketFd, &readSet);

	if(select(socketFd + 1, &readSet, NULL, NULL, NULL) < 0)
    {
        std::cout << "Socket error. Exiting." << std::endl;
        return -1;
    }

    if(FD_ISSET(socketFd, &readSet))
    {
    	//Perform the read and process the packet
    	if(readPacketsToBuffer(socketFd) > 0)
    	{
    		return processQueuedPackets(socketFd);
    	}

    	else
    	{
    		std::cout << "Connection could not be established." << std::endl;
    		return -1;
    	}
    }

    return 1;
}

void RDTManager::updateReceiverPacketBufferSize(unsigned int senderSize)
{
	delete[] mReceivePacketBuffer;
	mReceivePacketBufferSize = senderSize/RDTManager::MAX_PACKET_SIZE;
	if( (int) (senderSize % RDTManager::MAX_PACKET_SIZE - mHeaderSize) > 0) //check if we will need one extra packet to send extra data
	{								
		mReceivePacketBufferSize++;
	}

	mRemoteCwnd = senderSize;

	mReceivePacketBuffer = new RDTPacket[mReceivePacketBufferSize];
	std::cout << "Receive Packet Buffer size is now: " << mReceivePacketBufferSize << " packets" << std::endl;
}

int RDTManager::sendAck(int socketFd, double ackNumber)
{	
	RDTPacketHeader pktHeader;
	int bytesInPacket;
	double sequenceNumber;

	getNextSequenceNumber(false, 0, bytesInPacket, sequenceNumber);

	setPacketHeader(&pktHeader, sequenceNumber, ackNumber, mCwnd, 0, true, false, false);
	
	return sendPacket(socketFd, &pktHeader, NULL);
}


int RDTManager::processQueuedPackets(int socketFd)
{
	for(int i = 0; i < mQueuedPackets.size(); i++)
	{
		if(processPacket(socketFd, &(mQueuedPackets[i].header), mQueuedPackets[i].data, i) < 0)
		{
			return -1;
		}
	}

	mQueuedPackets.clear();
	return 1;

}

int RDTManager::senderHandleAckPacket(int socketFd, RDTPacketHeader* pktHeader, char* data, int bufferIndex)
{
	if(pktHeader->ackNumber >= mSenderBase && pktHeader->ackNumber < mSenderBase + mCwnd)
	{
		mSentPackets.erase(mSentPackets.begin() + bufferIndex); //remove packet from sent packet list since we have acked it

		if(pktHeader->ackNumber == mSenderBase)
		{
			double smallestUnackedSequenceNumber;
			for(int i = 0; i < mSentPackets.size(); i++)
			{
				if(!mSentPackets[i].acked)
				{
					smallestUnackedSequenceNumber = mSentPackets[i].header.sequenceNumber; //get first smallest unacked packet seq num
					break;
				}
			}

			for(int i = 0; i < mSentPackets.size(); i++)
			{
				if(!mSentPackets[i].acked && smallestUnackedSequenceNumber > mSentPackets[i].header.sequenceNumber)
				{
					smallestUnackedSequenceNumber = mSentPackets[i].header.sequenceNumber;
				}
			}

			mSenderBase = smallestUnackedSequenceNumber;
		}
	}

	return 1;
}

int RDTManager::receiverHandleDataPacket(int socketFd, RDTPacketHeader* pktHeader, char* data, int bufferIndex)
{
	//if packet is within window
	if(pktHeader->sequenceNumber >= mReceiverBase && pktHeader->sequenceNumber < mReceiverBase + mRemoteCwnd)
	{
		if(pktHeader->sequenceNumber == mReceiverBase)
		{
			int bytesWrittenToApplication = 0;
			double currentSequenceNumber = pktHeader->sequenceNumber;
			bool sequentialBufferedPackets = true;

			memcpy(mWriteBuffer + mWriteBufferOffset, data, pktHeader->dataSize);
			mWriteBufferOffset += pktHeader->dataSize;
			bytesWrittenToApplication += pktHeader->dataSize;

			while(sequentialBufferedPackets)
			{
				sequentialBufferedPackets = false;
				for(int i = 0; i < mBufferedPackets.size(); i++)
				{
					if(mBufferedPackets[i].header.sequenceNumber <= currentSequenceNumber + RDTManager::MAX_PACKET_SIZE 
						&& currentSequenceNumber != mBufferedPackets[i].header.sequenceNumber) //sequential buffered packet
					{
						memcpy(mWriteBuffer + mWriteBufferOffset, mBufferedPackets[i].data, mBufferedPackets[i].header.dataSize);
						mWriteBufferOffset += mBufferedPackets[i].header.dataSize;
						bytesWrittenToApplication += mBufferedPackets[i].header.dataSize;
						currentSequenceNumber = mBufferedPackets[i].header.sequenceNumber;
						sequentialBufferedPackets = true;
						mBufferedPackets.erase(mBufferedPackets.begin() + i); //packet is no longer buffered
						break;
					}
				}			
			}

			mReceiverBase += bytesWrittenToApplication;
		}

		else //buffer packet
		{
			double packetSequenceNumber = pktHeader->sequenceNumber;
			bool duplicateSequence = false;

			for(int i = 0; i < mBufferedPackets.size(); i++)
			{
				if(packetSequenceNumber == mBufferedPackets[i].header.sequenceNumber)
				{
					duplicateSequence = true;
					break;
				}
			}

			if(!duplicateSequence)
			{

				RDTPacket rdtPacket;

				memcpy(&rdtPacket.header, pktHeader, mHeaderSize);
				memcpy(rdtPacket.data, data, pktHeader->dataSize);
				rdtPacket.lost = false;
				rdtPacket.corrupt = false;
				rdtPacket.acked = false;
				rdtPacket.received = false;

				mBufferedPackets.push_back(rdtPacket);
			}
		}

		return sendAck(socketFd, pktHeader->sequenceNumber);
	}

	//outside of sequenceNumber
	if(pktHeader->sequenceNumber >= mReceiverBase - mRemoteCwnd && pktHeader->sequenceNumber < mReceiverBase)
	{
		return sendAck(socketFd, pktHeader->sequenceNumber);
	}

	//else ignore

	return 1;

}


int RDTManager::processPacket(int socketFd, RDTPacketHeader* pktHeader, char* data, int bufferIndex)
{
	//For Sender:
	//received SYN Packet
	if(pktHeader->synFlag && !pktHeader->ackFlag && !pktHeader->finFlag)
	{
		std::cout << "Received SYN packet" << std::endl;
		return serverAccept(socketFd, pktHeader, data);
	}
	//Received ACK packet
	if(!pktHeader->synFlag && pktHeader->ackFlag && !pktHeader->finFlag)
	{
		std::cout << "Received ACK packet" << std::endl;
		return senderHandleAckPacket(socketFd, pktHeader, data, bufferIndex);
	}

	//For Receivers:
	//Received SYNACK Packet
	if(pktHeader->synFlag && pktHeader->ackFlag && !pktHeader->finFlag)
	{
		double sequenceNumber = pktHeader->sequenceNumber;
		updateReceiverPacketBufferSize(pktHeader->senderSize);
		return sendAck(socketFd, sequenceNumber);
	}

	//Received Data Packet
	if(!pktHeader->synFlag && !pktHeader->ackFlag && !pktHeader->finFlag)
	{
		std::cout << "Received DATA packet" << std::endl;
		return receiverHandleDataPacket(socketFd, pktHeader,data, bufferIndex);
	}

	std::cout << "Could not process packet" << std::endl;
	return -1; //could not process packet
}


bool RDTManager::getNextSequenceNumber(bool dataPresent, double dataLeft, int &bytesInPacket, double &sequenceNumber)
{
	if(!dataPresent)
	{
		bytesInPacket = 0;
		sequenceNumber = mLastSequenceNumber + mHeaderSize; //we are only sending header if no data is present
		mLastSequenceNumber = sequenceNumber;
	}

	else if(mSenderBase + mCwnd - mLastSequenceNumber > 0)
	{
		bytesInPacket = RDTManager::MAX_PACKET_SIZE - mHeaderSize; //nominally fill packet up with bytes

		if(mSenderBase + mCwnd - mLastSequenceNumber  < RDTManager::MAX_PACKET_SIZE - mHeaderSize)
		{
			bytesInPacket = mSenderBase + mCwnd - mLastSequenceNumber; //if filling up with full bytes is bigger than window, send up to window size
		}

		if(dataLeft < bytesInPacket)
		{
			bytesInPacket = dataLeft;
		}

		sequenceNumber = mLastSequenceNumber + bytesInPacket + mHeaderSize;
		mLastSequenceNumber = sequenceNumber;
	}

	else
	{
		return false; //window does not allow a new sequence number right now
	}

	return true;
}

ssize_t RDTManager::sendPacket(int socketFd, RDTPacketHeader* pktHeader, char* data)
{
	ssize_t bytesWritten;
	socklen_t  addressSize = sizeof mRemoteAddress;

	//copy packet header and data into output buffer
	memset(mSendBuffer,0, RDTManager::MAX_PACKET_SIZE);
	memcpy(mSendBuffer, pktHeader, mHeaderSize);
	memcpy(mSendBuffer, data, pktHeader->dataSize);

	//Write bytes on socket
	bytesWritten = sendto(socketFd, mSendBuffer, mHeaderSize + pktHeader->dataSize, 0, (struct sockaddr *) &mRemoteAddress, addressSize);

	//Add packet to sentBuffer if it is a data packet
	if(!pktHeader->ackFlag && !pktHeader->synFlag && !pktHeader->finFlag)
	{
		RDTPacket rdtPacket;

		memcpy(&rdtPacket.header, pktHeader, mHeaderSize);
		memcpy(rdtPacket.data, data, pktHeader->dataSize);
		rdtPacket.lost = false;
		rdtPacket.corrupt = false;
		rdtPacket.acked = false;
		rdtPacket.received = false;

		mSentPackets.push_back(rdtPacket);
	}

	//Print packet header we just sent
	printPacketHeader(pktHeader,false);
	std::cout << "Wrote " << bytesWritten << " bytes." << std::endl;
	return bytesWritten - mHeaderSize;
}

ssize_t RDTManager::readPacketsToBuffer(int socketFd)
{
	RDTPacketHeader* pktHeader;
	RDTPacket rdtPacket;
	ssize_t bytesRead = 0;
	ssize_t totalBytesRead = 0;
	socklen_t addressSize = sizeof mRemoteAddress;
	char* receiveBufferPointer = mReceiveBuffer;
	char* data;
	//mNumberOfQueuedPackets = 0;

	///memset(mReceivePacketBuffer,0, sizeof(RDTManager::RDTPacket) * mReceivePacketBufferSize);

	do
	{
		//Clear buffer
		memset(mReceiveBuffer, 0, RDTManager::MAX_PACKET_SIZE);

		//Read header
	 	bytesRead = recvfrom(socketFd, receiveBufferPointer, mHeaderSize, 0, (struct sockaddr *) &mRemoteAddress, &addressSize);
	 	if(bytesRead > 0)
	 	{
	 		pktHeader = &rdtPacket.header; //point to header
			data = rdtPacket.data; //point to data
	 		memcpy(pktHeader, mReceiveBuffer, mHeaderSize);
	 		printPacketHeader(pktHeader, true);
	 		if(pktHeader->dataSize > 0)
	 			bytesRead += recvfrom(socketFd, data, pktHeader->dataSize, 0, (struct sockaddr *) &mRemoteAddress, &addressSize);
	 		std::cout << "Read " << bytesRead << " bytes." << std::endl;
	 		totalBytesRead += bytesRead;

	 		rdtPacket.lost     = false;
			rdtPacket.corrupt  = false;
			rdtPacket.acked    = false;
			rdtPacket.received = false;

	 		mQueuedPackets.push_back(rdtPacket);
	 		//mNumberOfQueuedPackets++;
	 	}

	}while(bytesRead > 0 && mQueuedPackets.size() < mReceivePacketBufferSize);

	return totalBytesRead;
}


void RDTManager::printPacketHeader(RDTPacketHeader* pktHeader, bool received)
{
	std::cout << std::endl;

	if(received)
		std::cout << "RECEIVED PACKET: " << std::endl;

	else
		std::cout << "SENT PACKET: " << std::endl;

	std::cout << "Sequence Number: " << pktHeader->sequenceNumber << std::endl;
	std::cout << "Ack Number: " << pktHeader->ackNumber << std::endl;
	std::cout << "Data Size: " << pktHeader->dataSize << std::endl;
	std::cout << "Sender Window: " << pktHeader->senderSize << std::endl;
	std::cout << "ACK Flag: " << pktHeader->ackFlag << std::endl;
	std::cout << "SYN Flag: " << pktHeader->synFlag << std::endl;
	std::cout << "FIN Flag: " << pktHeader->finFlag << std::endl;
}

void RDTManager::setPacketHeader(
								RDTPacketHeader* pktHeader,
								double sequenceNumber,
								double ackNumber,
								unsigned int senderSize,
								unsigned int dataSize,
								bool ackFlag,
								bool synFlag,
								bool finFlag
								)
{

	memset(pktHeader, 0, mHeaderSize);
 	pktHeader->sequenceNumber = sequenceNumber;
 	pktHeader->ackNumber = ackNumber;
 	pktHeader->senderSize = senderSize;
 	pktHeader->dataSize = dataSize;
 	pktHeader->ackFlag = ackFlag;
 	pktHeader->synFlag = synFlag;
 	pktHeader->finFlag = finFlag;

}

//called by application
ssize_t RDTManager::readFromSocket(int socketFd, char* outBuffer, int outBufferSize)
{
	ssize_t bytesWritten;
	RDTPacket rdtPacket;
	int dataLeft = outBufferSize;
	int bytesInPacket;
	fd_set activeFdSet;
	fd_set readSet;
	fd_set writeSet;

	mWriteBuffer = outBuffer; //set where to write data that is received.
	mWriteBufferOffset = 0;

	FD_ZERO(&activeFdSet);
	FD_ZERO(&readSet);
	FD_ZERO(&writeSet);
	FD_SET(socketFd, &activeFdSet);
	FD_SET(socketFd, &readSet);
	FD_SET(socketFd, &writeSet);

 	while(mWriteBufferOffset < outBufferSize)
 	{
		if(select(socketFd + 1, &readSet, NULL, NULL, NULL) < 0) //if something to read
		{
			std::cout << "Socket Error in readFromSocket() waiting for read.";
			return -1;
		}

		if(FD_ISSET(socketFd, &readSet)) //our socket is ready to read
		{
			readPacketsToBuffer(socketFd);
			processQueuedPackets(socketFd);
		}
	}

	return 0;
}

//called by application
ssize_t RDTManager::writeToSocket(int socketFd, char* inBuffer, int inBufferSize)
{
	ssize_t bytesWritten;
	ssize_t totalBytesWritten = 0;
	double nextSequenceNumber;
	RDTPacket rdtPacket;
	int dataLeft = inBufferSize;
	int bytesInPacket;
	fd_set activeFdSet;
	fd_set readSet;
	fd_set writeSet;

	FD_ZERO(&activeFdSet);
	FD_ZERO(&readSet);
	FD_ZERO(&writeSet);
	FD_SET(socketFd, &activeFdSet);
	FD_SET(socketFd, &readSet);
	FD_SET(socketFd, &writeSet);

	while(bytesWritten < inBufferSize)
	{
		//split input buffer up into multiple packets and send
		if(select(socketFd + 1, NULL, &writeSet, NULL, NULL) < 0)
		{
			std::cout << "Socket Error in writeToSocket() waiting for write.";
			return -1;
		}

		if(FD_ISSET(socketFd,&writeSet)) //if ready to write to socket
		{
			while(getNextSequenceNumber(true, dataLeft, bytesInPacket, nextSequenceNumber)) //if we can send some packet
			{

				memset(&rdtPacket, 0, sizeof(rdtPacket)); //clear rdtPacket
				setPacketHeader(&rdtPacket.header, nextSequenceNumber, 0, mCwnd, bytesInPacket, false, false, false);
				memcpy(rdtPacket.data, inBuffer + bytesWritten, bytesInPacket);
				bytesWritten = sendPacket(socketFd, &rdtPacket.header, rdtPacket.data);
				if(bytesWritten < 0)
				{
					std::cout << "Error sending packet in writeToSocket()." << std::endl;
					return -1;
				}

				dataLeft -= bytesWritten;
				totalBytesWritten += bytesWritten;
			}

			if(select(socketFd + 1, &readSet, NULL, NULL, NULL) < 0) //check if socket is ready for reading
			{
				std::cout << "Socket Error in writeToSocket() waiting for read." << std::endl;
			}

			if(FD_ISSET(socketFd, &readSet))
			{
				readPacketsToBuffer(socketFd);
				processQueuedPackets(socketFd);
				checkTimedOutPackets();
			}
		}
	}
	
	return totalBytesWritten;
}


int RDTManager::checkTimedOutPackets()
{
	/*For each packet in sentPacket List*/
	//If currentTime > timeSent + timeOut
	//  sendPacket(pktHeader,pktData)
}


// if mPktLossProb==0 no packets are dropped.
bool RDTManager::dropPacket()
{
    int randomNumber = rand()%101;  
    int threshold = (int)( mPktLossProb*100); 
    
    if (mPktLossProb != 0 && randomNumber <=  threshold)
        return true; // drop the packets below threshold.
     return false;
}

// if mPktCorruptProb==0 no packets are corrupted.
bool RDTManager::corruptPacket()
{
    int randomNumber = rand()%101;  
    int threshold = (int)( mPktCorruptProb*100); 
    
    if (mPktCorruptProb != 0 && randomNumber <=  threshold)
        return true; // corrupt the packets below threshold.
     return false;
}

