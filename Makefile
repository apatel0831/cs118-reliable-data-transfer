# This makefile builds receiver and sender

# Compiler
CC=g++

# Compiler flags
CFLAGS= -g -std=c++0x

all:
	@echo
	@echo *********Building Receiver....
	@$(CC) $(CFLAGS) ./receiver/receiver.cpp ./shared/RDTManager.cpp ./shared/ApplicationLayer.cpp -o ./receiver/receiver
	@echo *********Buiding Sender....
	@$(CC) $(CFLAGS) ./sender/sender.cpp ./shared/RDTManager.cpp ./shared/ApplicationLayer.cpp -o ./sender/sender
	@echo Done...
	
	
clean:
	@echo
	@echo Cleaning all *.o files ...
	@rm -fr *.o
	@echo Cleaning sender...
	@rm -fr ./sender/sender 
	@echo Cleaning receiver...
	@rm -fr ./receiver/receiver
